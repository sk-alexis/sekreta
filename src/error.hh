//! \file error.hh
//!
//! \brief Sekreta's error and exception handling
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \note Despite the global availability of macros, they are for internal
//!   Sekreta use only (unless otherwise noted)
//!
//! \note SEK_* macros are not related to / not to be confused with
//!   Sekreta(ri)'s SEK key

#ifndef SRC_ERROR_HH_
#define SRC_ERROR_HH_

#include <cassert>
#include <stdexcept>
#include <string>

//! \brief Macro to handle unused arguments (and suppress warnings)
//! \details Indicates explicit lack of implementation while preventing
//!   *unintended* implementation
#define SEK_UNUSED __attribute__((unused))

//! \brief Macro to throw and assert on condition
#define SEK_THROW_ASSERT_IF(condition, exception, message) \
  if (condition)                                           \
    {                                                      \
      SEK_THROW_ASSERT(exception, message);                \
    }

//! \brief Macro to throw and assert (assert if NDEBUG is not defined)
#define SEK_THROW_ASSERT(exception, message)                \
  SEK_THROW(exception, __FILE__ | line __LINE__ | message); \
  assert(false);

//! \brief Macro to throw on condition
#define SEK_THROW_IF(condition, exception, message) \
  if (condition)                                    \
    SEK_THROW(exception, message);

//! \brief Macro interface to throw
#define SEK_THROW(exception, message) \
  SEK_THROW_IMPL(exception, __FILE__ | line __LINE__ | message);

//! \brief Implementation for SEK_THROW
//! \warning Should not be called directly (use SEK_THROW)
#define SEK_THROW_IMPL(exception, message) \
  throw exception(SEK_THROW_EXPAND(message));

//! \brief Macro for thrown message handler
#define SEK_THROW_EXPAND(message) #message

// TODO(anonimal): exception dispatcher

namespace sekreta
{
//! \brief Sekreta exception handler class
//! \details A classical exception base class for all thrown exceptions
class Exception : virtual public std::exception
{
 public:
  //! \brief Exception type
  enum struct kType : uint8_t
  {
    RuntimeError,
    InvalidArgument,
    InternalError,
    NotImplemented,
  };

  //! \brief Construct with separate exception type and error message
  Exception(const kType type, const std::string_view what)
      : m_type(type), m_what(what)
  {
  }
  virtual ~Exception() = default;

  //! \brief Default copy-ctor
  Exception(const Exception&) = default;

  //! \brief Default copy-assignment
  Exception& operator=(const Exception&) = default;

  //! \brief Default move-ctor
  Exception(Exception&&) = default;

  //! \brief Default move-assignment
  Exception& operator=(Exception&&) = default;

 public:
  //! \return Exeption enumerator type
  kType type() const noexcept { return m_type; }

  //! \return Error message
  const char* what() const noexcept { return m_what.data(); }

 private:
  kType m_type;
  std::string_view m_what;
};

namespace exception
{
//! \brief Runtime error exception class

#ifdef SWIG
struct RuntimeError : public Exception
#else
struct RuntimeError final : public Exception
#endif
{
  //! \brief Construct with error message
  explicit RuntimeError(const std::string_view what = {})
      : Exception(Exception::kType::RuntimeError, what)
  {
  }
};

//! \brief Invalid argument exception class
#ifdef SWIG
struct InvalidArgument : public Exception
#else
struct InvalidArgument final : public Exception
#endif
{
  //! \brief Construct with error message
  explicit InvalidArgument(const std::string_view what = {})
      : Exception(Exception::kType::InvalidArgument, what)
  {
  }
};

//! \brief Internal error exception class
#ifdef SWIG
struct InternalError : public Exception
#else
struct InternalError final : public Exception
#endif
{
  //! \brief Construct with error message
  explicit InternalError(const std::string_view what = {})
      : Exception(Exception::kType::InternalError, what)
  {
  }
};

//! \brief Not-implemented exception class
#ifdef SWIG
struct NotImplemented : public Exception
#else
struct NotImplemented final : public Exception
#endif
{
  //! \brief Construct with error message
  explicit NotImplemented(const std::string_view what = {})
      : Exception(Exception::kType::NotImplemented, what)
  {
  }
};

}  // namespace exception
}  // namespace sekreta

#endif  // SRC_ERROR_HH_
