//! \file sekretari/4se.hh
//!
//! \brief Implements the Sekretari 4SE component
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \todo Merge WIP branch when in working condition

#ifndef SRC_SEKRETARI_4SE_HH_
#define SRC_SEKRETARI_4SE_HH_

#endif  // SRC_SEKRETARI_4SE_HH_
