// Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "generic.hh"

#include <string>
#include <string_view>
#include <thread>
#include <variant>

#include "gtest/gtest.h"

namespace type = sekreta::type;

struct AbstractFactoryFixture : ::testing::Test
{
 protected:
  enum struct kKey
  {
    One,
    Two,
    Three,
  };

 protected:
  template <typename t_derived>
  class MyCustomFactory
  {
   public:
    bool create() const
    {
      return static_cast<const t_derived*>(this)->create_impl();
    }
  };

  class MyCustomObject final : public MyCustomFactory<MyCustomObject>
  {
   public:
    bool create_impl() const { return true; }
  };

  struct GenericObject
  {
  };

  struct AnotherObject
  {
  };

 protected:
  template <
      typename t_key,
      typename t_factory,
      template <typename...>
      typename t_container>
  class AbstractFactory final
      : public ::sekreta::internal::
            AbstractFactory<t_key, t_factory, t_container>
  {
  };

  using t_key = kKey;
  using t_factory = std::variant<MyCustomObject, GenericObject, AnotherObject>;

  template <template <typename...> typename t_container>
  using t_abstract = AbstractFactory<t_key, t_factory, t_container>;

  using t_first = MyCustomObject;
  using t_second = GenericObject;
  using t_third = AnotherObject;
};

TEST_F(AbstractFactoryFixture, Map_CreateInstance)
{
  t_abstract<std::map> factory;
  kKey key = kKey::One;

  EXPECT_NO_THROW(factory.create<t_first>(key));
  EXPECT_EQ(0, factory.size());

  auto system = factory.create<t_first>(key);
  EXPECT_NE(nullptr, system);
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, MultiMap_CreateInstance)
{
  t_abstract<std::multimap> factory;
  kKey key = kKey::One;

  EXPECT_NO_THROW(factory.create<t_first>(key));
  EXPECT_EQ(0, factory.size());

  auto system = factory.create<t_first>(key);
  EXPECT_NE(nullptr, system);
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, Map_CustomFactory)
{
  t_abstract<std::map> factory;
  auto custom = factory.create<t_first>(kKey::One);
  EXPECT_EQ(true, custom->create());
}

TEST_F(AbstractFactoryFixture, MultiMap_CustomFactory)
{
  t_abstract<std::multimap> factory;
  auto custom = factory.create<t_first>(kKey::One);
  EXPECT_EQ(true, custom->create());
}

TEST_F(AbstractFactoryFixture, Map_CustomKey_Create)
{
  using t_key = type::UID<std::string>;
  AbstractFactory<t_key, t_factory, std::map> factory;
  t_key key("key");

  EXPECT_NO_THROW(factory.create<t_first>(key));
  EXPECT_EQ(0, factory.size());

  auto anon = factory.create<t_first>(key);
  EXPECT_NE(nullptr, anon);
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, MultiMap_CustomKey_Create)
{
  using t_key = type::UID<std::string>;
  AbstractFactory<t_key, t_factory, std::multimap> factory;
  t_key key("key");

  EXPECT_NO_THROW(factory.create<t_first>(key));
  EXPECT_EQ(0, factory.size());

  auto anon = factory.create<t_first>(key);
  EXPECT_NE(nullptr, anon);
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, Map_CustomKey_CheckInAndOut)
{
  enum struct Custom : uint8_t
  {
    Enumeration,
  };

  using t_key = Custom;
  t_key key = Custom::Enumeration;
  AbstractFactory<t_key, t_factory, std::map> factory;

  EXPECT_NO_THROW(factory.check_in<t_first>(key));
  EXPECT_EQ(1, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key));
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, MultiMap_CustomKey_CheckInAndOut)
{
  enum struct Custom : uint8_t
  {
    Enumeration,
  };

  using t_key = Custom;
  t_key key = Custom::Enumeration;
  AbstractFactory<t_key, t_factory, std::multimap> factory;

  EXPECT_NO_THROW(factory.check_in<t_first>(key));
  EXPECT_EQ(1, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key));
  EXPECT_EQ(0, factory.size());

  EXPECT_THROW(factory.check_out<t_first>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, Map_SingleSystem_MultipleCheckInAndOut)
{
  t_abstract<std::map> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  factory.check_in<t_first>(key1);
  factory.check_in<t_first>(key2);
  EXPECT_EQ(2, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key1));
  EXPECT_NO_THROW(factory.check_out<t_first>(key2));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, MultiMap_SingleSystem_MultipleCheckInAndOut)
{
  t_abstract<std::multimap> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  factory.check_in<t_first>(key1);
  factory.check_in<t_first>(key2);
  EXPECT_EQ(2, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key1));
  EXPECT_NO_THROW(factory.check_out<t_first>(key2));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, Map_MultipleSystem_MultipleCheckInAndOut)
{
  t_abstract<std::map> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  factory.check_in<t_first>(key1);
  factory.check_in<t_second>(key2);
  EXPECT_EQ(2, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key1));
  EXPECT_NO_THROW(factory.check_out<t_second>(key2));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, MultiMap_MultipleSystem_MultipleCheckInAndOut)
{
  t_abstract<std::multimap> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  factory.check_in<t_first>(key1);
  factory.check_in<t_second>(key2);
  EXPECT_EQ(2, factory.size());

  EXPECT_NO_THROW(factory.check_out<t_first>(key1));
  EXPECT_NO_THROW(factory.check_out<t_second>(key2));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, Map_CopyAssignment)
{
  t_abstract<std::map> one, two;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  one.check_in<t_first>(key1);
  one.check_in<t_first>(key2);

  two = one;

  EXPECT_EQ(true, two.erase(key1));
  EXPECT_EQ(true, two.erase(key2));
}

TEST_F(AbstractFactoryFixture, MultiMap_CopyAssignment)
{
  t_abstract<std::multimap> one, two;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  one.check_in<t_first>(key1);
  one.check_in<t_first>(key2);

  two = one;

  EXPECT_EQ(true, two.erase<t_first>(key1));
  EXPECT_EQ(true, two.erase<t_first>(key2));
}

TEST_F(AbstractFactoryFixture, Map_CopyCtor)
{
  t_abstract<std::map> one;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  one.check_in<t_first>(key1);
  one.check_in<t_first>(key2);

  t_abstract<std::map> two(one);

  EXPECT_EQ(true, two.erase(key1));
  EXPECT_EQ(true, two.erase(key2));
}

TEST_F(AbstractFactoryFixture, MultiMap_CopyCtor)
{
  t_abstract<std::multimap> one;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;

  one.check_in<t_first>(key1);
  one.check_in<t_first>(key2);

  t_abstract<std::multimap> two(one);

  EXPECT_EQ(true, two.erase<t_first>(key1));
  EXPECT_EQ(true, two.erase<t_first>(key2));
}

TEST_F(AbstractFactoryFixture, Map_CheckoutWithoutCheckin)
{
  t_abstract<std::map> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;
  kKey key3 = kKey::Three;

  factory.check_in<t_first>(key1);
  factory.check_in<t_second>(key2);
  EXPECT_EQ(2, factory.size());

  EXPECT_THROW(factory.check_out<t_third>(key3), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, MultiMap_CheckoutWithoutCheckin)
{
  t_abstract<std::multimap> factory;
  kKey key = kKey::One;

  factory.check_in<t_first>(key);
  factory.check_in<t_second>(key);
  EXPECT_EQ(2, factory.size());

  EXPECT_THROW(factory.check_out<t_third>(key), sekreta::Exception);
}

TEST_F(AbstractFactoryFixture, Map_EraseInstance)
{
  t_abstract<std::map> factory;
  kKey key = kKey::One;

  EXPECT_EQ(0, factory.size());

  factory.check_in<t_first>(key);
  EXPECT_EQ(1, factory.size());

  EXPECT_EQ(true, factory.erase(key));
  EXPECT_EQ(0, factory.size());

  // Does not exist
  EXPECT_EQ(0, factory.erase(kKey::Two));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, MultiMap_EraseInstance)
{
  t_abstract<std::multimap> factory;
  kKey key = kKey::One;

  EXPECT_EQ(0, factory.size());

  factory.check_in<t_first>(key);
  EXPECT_EQ(1, factory.size());

  EXPECT_EQ(true, factory.erase<t_first>(key));
  EXPECT_EQ(0, factory.size());

  // Does not exist
  EXPECT_EQ(0, factory.erase<t_first>(kKey::Two));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, Map_EraseMultipleInstances)
{
  t_abstract<std::map> factory;
  kKey key1 = kKey::One;
  kKey key2 = kKey::Two;
  kKey key3 = kKey::Three;

  factory.check_in<t_first>(key1);
  factory.check_in<t_second>(key2);
  factory.check_in<t_third>(key3);

  EXPECT_EQ(true, factory.erase(key1));
  EXPECT_EQ(2, factory.size());

  EXPECT_EQ(true, factory.erase(key2));
  EXPECT_EQ(1, factory.size());

  EXPECT_EQ(true, factory.erase(key3));
  EXPECT_EQ(0, factory.size());

  EXPECT_EQ(false, factory.erase(key3));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, MultiMap_EraseMultipleInstances)
{
  t_abstract<std::multimap> factory;
  kKey key = kKey::One;

  factory.check_in<t_first>(key);
  factory.check_in<t_second>(key);
  factory.check_in<t_third>(key);

  EXPECT_EQ(true, factory.erase<t_first>(key));
  EXPECT_EQ(2, factory.size());

  EXPECT_EQ(true, factory.erase<t_second>(key));
  EXPECT_EQ(1, factory.size());

  EXPECT_EQ(true, factory.erase<t_third>(key));
  EXPECT_EQ(0, factory.size());

  EXPECT_EQ(false, factory.erase<t_third>(key));
  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, Map_Thread)
{
  using t_key = size_t;
  AbstractFactory<t_key, t_factory, std::map> factory;
  t_key key{};

  auto check_in = [&](const t_key& key) {
    EXPECT_NO_THROW(factory.check_in<t_first>(key));
  };

  std::thread thread([&]() {
    for (size_t i = 0; i < 100; i++)
      check_in(i);
  });

  // And also run in main thread
  for (size_t i = 100; i < 200; i++)
    check_in(i);

  thread.join();

  EXPECT_EQ(200, factory.size());

  for (size_t i = 0; i < 200; i++)
    EXPECT_EQ(true, factory.erase(i));

  EXPECT_EQ(0, factory.size());
}

TEST_F(AbstractFactoryFixture, MultiMap_Thread)
{
  using t_key = size_t;
  AbstractFactory<t_key, t_factory, std::multimap> factory;
  t_key key{};

  auto check_in = [&](const t_key& key) {
    EXPECT_NO_THROW(factory.check_in<t_first>(key));
  };

  std::thread thread([&]() {
    for (size_t i = 0; i < 100; i++)
      check_in(i);
  });

  // And also run in main thread
  for (size_t i = 100; i < 200; i++)
    check_in(i);

  thread.join();

  EXPECT_EQ(200, factory.size());

  for (size_t i = 0; i < 200; i++)
    EXPECT_EQ(true, factory.erase<t_first>(i));

  EXPECT_EQ(0, factory.size());
}

struct AnyAbstractFactoryFixture : ::testing::Test
{
 protected:
  template <typename t_key, typename t_tag>
  class AbstractFactory
      : virtual public ::sekreta::internal::api::
            AbstractFactory<t_key, t_tag, std::any, std::multimap>
  {
  };

  template <typename t_key, typename t_tag>
  using Factory = AbstractFactory<t_key, t_tag>;

  Factory<std::string_view, type::kAPI> factory;

 protected:
  using t_key = type::kAPI;

  struct Library
  {
    static constexpr type::kAPI type = t_key::Library;
    int count{};
  };

  struct Socket
  {
    static constexpr type::kAPI type = t_key::Socket;
    int count{};
  };
};

TEST_F(AnyAbstractFactoryFixture, CheckIn_CheckOut)
{
  ASSERT_NO_THROW(factory.check_in<Library>("default"));
  std::shared_ptr<Library> one = factory.check_out<Library>("default");
  ASSERT_TRUE(one);
  ASSERT_EQ(one.use_count(), 1);
  ASSERT_EQ(one->type, t_key::Library);
  ASSERT_EQ(one->count, 0);
  ASSERT_EQ(factory.size(), 0);
  ASSERT_THROW(factory.check_out<Library>("default"), sekreta::Exception);

  one->count++;

  ASSERT_NO_THROW(factory.check_in<Library>("two", one));
  std::shared_ptr<Library> two = factory.check_out<Library>("two");
  EXPECT_TRUE(two);
  EXPECT_EQ(two.use_count(), 2);
  EXPECT_EQ(two->type, t_key::Library);
  EXPECT_EQ(two->count, 1);
  EXPECT_EQ(factory.size(), 0);
  EXPECT_THROW(factory.check_out<Library>("two"), sekreta::Exception);

  two->count++;

  ASSERT_EQ(two->count, 2);
  ASSERT_EQ(one->count, 2);
}

TEST_F(AnyAbstractFactoryFixture, Move)
{
  ASSERT_NO_THROW(factory.check_in<Library>("default"));
  std::shared_ptr<Library> one = factory.check_out<Library>("default");
  ASSERT_TRUE(one);
  ASSERT_EQ(one.use_count(), 1);
  ASSERT_EQ(one->type, t_key::Library);
  ASSERT_EQ(one->count, 0);
  ASSERT_EQ(factory.size(), 0);
  ASSERT_THROW(factory.check_out<Library>("default"), sekreta::Exception);

  one->count++;

  std::shared_ptr<Library> two = std::move(one);
  ASSERT_FALSE(one);
  EXPECT_TRUE(two);
  EXPECT_EQ(two.use_count(), 1);
  EXPECT_EQ(two->type, t_key::Library);
  EXPECT_EQ(two->count, 1);
  EXPECT_EQ(factory.size(), 0);
}

TEST_F(AnyAbstractFactoryFixture, Create)
{
  ASSERT_NO_THROW(factory.create<Socket>("stub"));

  std::shared_ptr<Socket> one = factory.create<Socket>("one");
  ASSERT_TRUE(one);
  ASSERT_EQ(one.use_count(), 1);
  ASSERT_EQ(one->type, t_key::Socket);
  ASSERT_EQ(one->count, 0);
  ASSERT_EQ(factory.size(), 0);

  std::shared_ptr<Socket> two = factory.create<Socket>("two");
  ASSERT_TRUE(two);
  ASSERT_EQ(two.use_count(), 1);
  ASSERT_EQ(two->type, t_key::Socket);
  ASSERT_EQ(two->count, 0);
  ASSERT_EQ(factory.size(), 0);
}

TEST_F(AnyAbstractFactoryFixture, Borrow)
{
  ASSERT_THROW(factory.borrow<Socket>("stub"), sekreta::Exception);
  factory.check_in<Socket>("stub");
  ASSERT_NO_THROW(factory.borrow<Socket>("stub"));

  factory.check_in<Library>("default");
  std::shared_ptr<Library> one = factory.borrow<Library>("default");
  ASSERT_TRUE(one);
  ASSERT_EQ(one.use_count(), 2);
  ASSERT_EQ(one->type, t_key::Library);
  ASSERT_EQ(one->count, 0);
  ASSERT_EQ(factory.size(), 2);

  one->count++;

  std::shared_ptr<Library> two = factory.borrow<Library>("default");
  EXPECT_TRUE(two);
  EXPECT_EQ(one.use_count(), 3);
  EXPECT_EQ(two->type, t_key::Library);
  EXPECT_EQ(two->count, 1);
  EXPECT_EQ(factory.size(), 2);

  EXPECT_TRUE(factory.erase<Library>("default"));
  EXPECT_EQ(factory.size(), 1);
  EXPECT_THROW(factory.borrow<Library>("default"), sekreta::Exception);
}

TEST_F(AnyAbstractFactoryFixture, SameKeyMultipleType)
{
  factory.check_in<Library>("default");
  factory.check_in<Socket>("default");

  std::shared_ptr<Library> lib = factory.borrow<Library>("default");
  ASSERT_EQ(lib->type, t_key::Library);

  std::shared_ptr<Socket> socket = factory.borrow<Socket>("default");
  ASSERT_EQ(socket->type, t_key::Socket);

  EXPECT_EQ(factory.size(), 2);
}

TEST_F(AnyAbstractFactoryFixture, Find)
{
  factory.check_in<Library>("default");
  factory.check_in<Socket>("default");

  ASSERT_TRUE(factory.find<Library>("default"));
  ASSERT_TRUE(factory.find<Socket>("default"));
  ASSERT_FALSE(factory.find<std::monostate>("default"));

  ASSERT_FALSE(factory.find<Library>("N/A"));
  ASSERT_FALSE(factory.find<Socket>("N/A"));
}

TEST_F(AnyAbstractFactoryFixture, View)
{
  factory.check_in<Library>("default");
  factory.check_in<Socket>("default");

  ASSERT_EQ(factory.size(), 2);
  ASSERT_EQ(factory.view().size(), 2);

  for (const auto& view : factory.view())
    {
      ASSERT_EQ(view.first, "default");
      ASSERT_TRUE(
          (view.second == type::kAPI::Library)
          || (view.second == type::kAPI::Socket));
    }
}

TEST_F(AnyAbstractFactoryFixture, Contains)
{
  factory.check_in<Library>("one");
  factory.check_in<Socket>("one");
  factory.check_in<Library>("two");
  factory.check_in<Socket>("two");

  ASSERT_TRUE(factory.contains<Library>("one"));
  ASSERT_TRUE(factory.contains<Socket>("one"));
  ASSERT_TRUE(factory.contains<Library>("two"));
  ASSERT_TRUE(factory.contains<Socket>("two"));

  ASSERT_FALSE(factory.contains<Library>("three"));
  ASSERT_FALSE(factory.contains<std::monostate>("one"));
}

TEST_F(AnyAbstractFactoryFixture, Erase_Clear)
{
  factory.check_in<Library>("one");
  factory.check_in<Library>("two");

  factory.check_in<Socket>("one");
  factory.check_in<Socket>("two");

  ASSERT_EQ(factory.size(), 4);
  EXPECT_EQ(factory.erase<Library>("two"), true);
  EXPECT_EQ(factory.size(), 3);
  EXPECT_EQ(factory.erase<Library>("two"), false);
  EXPECT_EQ(factory.size(), 3);
  EXPECT_EQ(factory.erase<Socket>("one"), true);
  EXPECT_EQ(factory.size(), 2);
  EXPECT_EQ(factory.erase<Socket>("one"), false);
  EXPECT_EQ(factory.size(), 2);

  factory.clear();
  EXPECT_EQ(factory.size(), 0);
}

struct MaestroFixture : ::testing::Test
{
 private:
  class SystemImpl final
  {
   public:
    std::string_view configure(const std::string_view arg) { return arg; }
    std::string_view configure() { return "configured"; }

    std::string_view start(const std::string_view arg) { return arg; }
    std::string_view start() { return "started"; }

    std::string_view restart(const std::string_view arg) { return arg; }
    std::string_view restart() { return "restarted"; }

    std::string_view status(const std::string_view arg) { return arg; }
    std::string_view status() { return "status'ed"; }

    std::string_view stop(const std::string_view arg) { return arg; }
    std::string_view stop() { return "stopped"; }
  };

  class MaestroImpl : public sekreta::internal::api::Maestro<MaestroImpl>
  {
   public:
    // Configure

    template <typename t_ret, typename t_arg>
    t_ret configure_impl(t_arg arg)
    {
      return m_impl.configure(arg);
    }

    template <typename t_ret>
    t_ret configure_impl()
    {
      return m_impl.configure();
    }

    // Start

    template <typename t_ret, typename t_arg>
    t_ret start_impl(t_arg arg)
    {
      return m_impl.start(arg);
    }

    template <typename t_ret>
    t_ret start_impl()
    {
      return m_impl.start();
    }

    // Restart

    template <typename t_ret, typename t_arg>
    t_ret restart_impl(t_arg arg)
    {
      return m_impl.restart(arg);
    }

    template <typename t_ret>
    t_ret restart_impl()
    {
      return m_impl.restart();
    }

    // Status

    template <typename t_ret, typename t_arg>
    t_ret status_impl(t_arg arg)
    {
      return m_impl.status(arg);
    }

    template <typename t_ret>
    t_ret status_impl()
    {
      return m_impl.status();
    }

    // Stop

    template <typename t_ret, typename t_arg>
    t_ret stop_impl(t_arg arg)
    {
      return m_impl.stop(arg);
    }

    template <typename t_ret>
    t_ret stop_impl()
    {
      return m_impl.stop();
    }

   private:
    SystemImpl m_impl;
  };

 protected:
  class Maestro final : public MaestroImpl
  {
    using t_impl = MaestroImpl;

   public:
    // Configure

    template <typename t_ret = std::string_view>
    t_ret configure()
    {
      return t_impl::template configure<t_ret>();
    }

    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret configure(t_arg arg)
    {
      return t_impl::template configure<t_ret, t_arg>(arg);
    }

    // Start

    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret start(t_arg arg)
    {
      return t_impl::template start<t_ret, t_arg>(arg);
    }

    template <typename t_ret = std::string_view>
    t_ret start()
    {
      return t_impl::template start<t_ret>();
    }

    // Restart

    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret restart(t_arg arg)
    {
      return t_impl::template restart<t_ret, t_arg>(arg);
    }

    template <typename t_ret = std::string_view>
    t_ret restart()
    {
      return t_impl::template restart<t_ret>();
    }

    // Status

    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret status(t_arg arg)
    {
      return t_impl::template status<t_ret, t_arg>(arg);
    }

    template <typename t_ret = std::string_view>
    t_ret status()
    {
      return t_impl::template status<t_ret>();
    }

    // Stop

    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret stop(t_arg arg)
    {
      return t_impl::template stop<t_ret, t_arg>(arg);
    }

    template <typename t_ret = std::string_view>
    t_ret stop()
    {
      return t_impl::template stop<t_ret>();
    }
  };
};

TEST_F(MaestroFixture, Maestro)
{
  using Spinelloccio = Maestro;
  Spinelloccio il_dottore;

  ASSERT_EQ(il_dottore.configure("L'è permesso"), "L'è permesso");
  ASSERT_EQ(il_dottore.configure(), "configured");

  ASSERT_EQ(
      il_dottore.start("Ha avuto il benefissio?"), "Ha avuto il benefissio?");
  ASSERT_EQ(il_dottore.start(), "started");

  ASSERT_EQ(
      il_dottore.restart("A che potensa l'è arrivata la scienza!"),
      "A che potensa l'è arrivata la scienza!");
  ASSERT_EQ(il_dottore.restart(), "restarted");

  ASSERT_EQ(
      il_dottore.status("Be', vediamo, vediamo!"), "Be', vediamo, vediamo!");
  ASSERT_EQ(il_dottore.status(), "status'ed");

  ASSERT_EQ(il_dottore.stop("ma io..."), "ma io...");
  ASSERT_EQ(il_dottore.stop(), "stopped");
}

struct LibraryFixture : ::testing::Test
{
 private:
  class SystemImpl final
  {
   public:
    std::string_view path(const std::string_view arg)
    {
      return !arg.empty() ? "I took the one less traveled by" : "";
    }

    std::string_view path() { return "And that has made all the difference."; }
  };

  class LibraryImpl : public sekreta::internal::api::Library<LibraryImpl>
  {
   public:
    template <typename t_ret, typename t_arg>
    t_ret path_impl(t_arg arg)
    {
      return m_impl.path(arg);
    }

    template <typename t_ret>
    t_ret path_impl()
    {
      return m_impl.path();
    }

   private:
    SystemImpl m_impl;
  };

 protected:
  class Library final : public LibraryImpl
  {
    using t_impl = LibraryImpl;

   public:
    template <
        typename t_ret = std::string_view,
        typename t_arg = const std::string_view>
    t_ret path(t_arg arg)
    {
      return t_impl::template path<t_ret, t_arg>(arg);
    }

    template <typename t_ret = std::string_view>
    t_ret path()
    {
      return t_impl::template path<t_ret>();
    }
  };
};

TEST_F(LibraryFixture, Library)
{
  Library lib;

  ASSERT_EQ(
      lib.path("Two roads diverged in a wood, and I-"),
      "I took the one less traveled by");

  ASSERT_EQ(lib.path(), "And that has made all the difference.");
}

struct StatusFixture : ::testing::Test
{
 private:
  class SystemImpl final
  {
   public:
    std::string_view version() { return "3.13.37"; }
    bool is_running() { return true; }
    std::string_view state() { return "Firewalled"; }
    uint64_t uptime() { return 8675309; }
    double inbound_bandwidth() { return 3.142857142; }
    double outbound_bandwidth() { return 85714285714; }
  };

  class StatusImpl : public sekreta::internal::api::Status<StatusImpl>
  {
   public:
    template <typename t_ret>
    t_ret version_impl()
    {
      return m_impl.version();
    }

    template <typename t_ret>
    t_ret is_running_impl()
    {
      return m_impl.is_running();
    }

    template <typename t_ret>
    t_ret state_impl()
    {
      return m_impl.state();
    }

    template <typename t_ret>
    t_ret uptime_impl()
    {
      return m_impl.uptime();
    }

    template <typename t_ret>
    t_ret inbound_bandwidth_impl()
    {
      return m_impl.inbound_bandwidth();
    }

    template <typename t_ret>
    t_ret outbound_bandwidth_impl()
    {
      return m_impl.outbound_bandwidth();
    }

   private:
    SystemImpl m_impl;
  };

 protected:
  class Status final : public StatusImpl
  {
    using t_impl = StatusImpl;

   public:
    template <typename t_ret = std::string_view>
    t_ret version()
    {
      return t_impl::template version_impl<t_ret>();
    }

    template <typename t_ret = bool>
    t_ret is_running()
    {
      return t_impl::template is_running_impl<t_ret>();
    }

    template <typename t_ret = std::string_view>
    t_ret state()
    {
      return t_impl::template state_impl<t_ret>();
    }

    template <typename t_ret = uint64_t>
    t_ret uptime()
    {
      return t_impl::template uptime_impl<t_ret>();
    }

    template <typename t_ret = double>
    t_ret inbound_bandwidth()
    {
      return t_impl::template inbound_bandwidth_impl<t_ret>();
    }

    template <typename t_ret = double>
    t_ret outbound_bandwidth()
    {
      return t_impl::template outbound_bandwidth_impl<t_ret>();
    }
  };
};

TEST_F(StatusFixture, Status)
{
  Status status;

  ASSERT_EQ(status.version(), "3.13.37");
  ASSERT_EQ(status.state(), "Firewalled");
  ASSERT_EQ(status.is_running(), true);
  ASSERT_EQ(status.uptime(), 8675309);
  ASSERT_EQ(status.inbound_bandwidth(), 3.142857142);
  ASSERT_EQ(status.outbound_bandwidth(), 85714285714);
}

struct RouterStatusFixture : ::testing::Test
{
 private:
  class SystemImpl final
  {
   public:
    std::string_view data_dir() { return "/home/feigenbaum/.bifurcation"; }
    double path_creation_rate() { return 4.669201; }
    uint32_t path_count() { return 609102990; }
    uint32_t active_peer_count() { return 671853203; }
    uint32_t known_peer_count() { return 820466201; }
    uint32_t floodfill_count() { return 617258185; }
    uint32_t leaseset_count() { return 577475768; }
  };

  class StatusImpl : public sekreta::internal::api::RouterStatus<StatusImpl>
  {
   public:
    template <typename t_ret>
    t_ret data_dir_impl()
    {
      return m_impl.data_dir();
    }

    template <typename t_ret>
    t_ret path_creation_rate_impl()
    {
      return m_impl.path_creation_rate();
    }

    template <typename t_ret>
    t_ret path_count_impl()
    {
      return m_impl.path_count();
    }

    template <typename t_ret>
    t_ret active_peer_count_impl()
    {
      return m_impl.active_peer_count();
    }

    template <typename t_ret>
    t_ret known_peer_count_impl()
    {
      return m_impl.known_peer_count();
    }

    // TODO(unassigned): This is network-specific
    template <typename t_ret>
    t_ret floodfill_count_impl()
    {
      return m_impl.floodfill_count();
    }

    // TODO(unassigned): This is network-specific
    template <typename t_ret>
    t_ret leaseset_count_impl()
    {
      return m_impl.leaseset_count();
    }

   private:
    SystemImpl m_impl;
  };

 protected:
  class Status final : public StatusImpl
  {
    using t_impl = StatusImpl;

   public:
    template <typename t_ret = std::string_view>
    t_ret data_dir()
    {
      return t_impl::template data_dir<t_ret>();
    }

    template <typename t_ret = double>
    t_ret path_creation_rate()
    {
      return t_impl::template path_creation_rate<t_ret>();
    }

    template <typename t_ret = uint32_t>
    t_ret path_count()
    {
      return t_impl::template path_count<t_ret>();
    }

    template <typename t_ret = uint32_t>
    t_ret active_peer_count()
    {
      return t_impl::template active_peer_count<t_ret>();
    }

    template <typename t_ret = uint32_t>
    t_ret known_peer_count()
    {
      return t_impl::template known_peer_count<t_ret>();
    }

    // TODO(unassigned): This is network-specific
    template <typename t_ret = uint32_t>
    t_ret floodfill_count()
    {
      return t_impl::template floodfill_count<t_ret>();
    }

    // TODO(unassigned): This is network-specific
    template <typename t_ret = uint32_t>
    t_ret leaseset_count()
    {
      return t_impl::template leaseset_count<t_ret>();
    }
  };
};

TEST_F(RouterStatusFixture, Status)
{
  Status status;

  ASSERT_EQ(status.data_dir(), "/home/feigenbaum/.bifurcation");
  ASSERT_EQ(status.path_creation_rate(), 4.669201);
  ASSERT_EQ(status.path_count(), 609102990);
  ASSERT_EQ(status.active_peer_count(), 671853203);
  ASSERT_EQ(status.known_peer_count(), 820466201);
  ASSERT_EQ(status.floodfill_count(), 617258185);
  ASSERT_EQ(status.leaseset_count(), 577475768);
}

struct PathStatusFixture : ::testing::Test
{
 private:
  class SystemImpl final
  {
   public:
    bool is_running() { return true; }
    double uptime() { return 0.4146825098511116; }
    std::string state() { return "prime numbers coded in binary"; }
  };

  class StatusImpl : public sekreta::internal::api::PathStatus<StatusImpl>
  {
   public:
    template <typename t_ret>
    t_ret is_running_impl()
    {
      return m_impl.is_running();
    }

    template <typename t_ret>
    t_ret uptime_impl()
    {
      return m_impl.uptime();
    }

    template <typename t_ret>
    t_ret state_impl()
    {
      return m_impl.state();
    }

   private:
    SystemImpl m_impl;
  };

 protected:
  class Status final : public StatusImpl
  {
    using t_impl = StatusImpl;

   public:
    template <typename t_ret = bool>
    t_ret is_running()
    {
      return t_impl::template is_running<t_ret>();
    }

    template <typename t_ret = double>
    t_ret uptime()
    {
      return t_impl::template uptime<t_ret>();
    }

    template <typename t_ret = std::string>
    t_ret state()
    {
      return t_impl::template state<t_ret>();
    }

    // etc.
  };
};

TEST_F(PathStatusFixture, Status)
{
  Status status;

  ASSERT_EQ(status.is_running(), true);
  ASSERT_EQ(status.uptime(), 0.4146825098511116);
  ASSERT_EQ(status.state(), "prime numbers coded in binary");
}

// TODO(anonimal): more + better concurrency tests
// TODO: std::promise + std::future
