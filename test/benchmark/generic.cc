// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "generic.hh"

#include <benchmark/benchmark.h>

namespace type = sekreta::type;

class GenericAbstractFactory : public benchmark::Fixture
{
 protected:
  template <
      typename t_key,
      typename t_factory,
      template <typename...>
      typename t_container>
  class AbstractFactory : virtual public ::sekreta::internal::
                              AbstractFactory<t_key, t_factory, t_container>
  {
  };

  enum struct kKey
  {
    One,
    Two,
    Three,
  };

  struct Object
  {
  };

  using t_key = uint32_t;
  using t_object = Object;
  using t_factory = std::variant<std::monostate, t_object>;

  template <template <typename...> typename t_container>
  using t_abstract = AbstractFactory<t_key, t_factory, t_container>;

  t_key key{};
};

// TODO(anonimal): timings commented out until this is resolved:
//   https://github.com/google/benchmark/issues/797

BENCHMARK_F(GenericAbstractFactory, Map_CheckIn_Single)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    factory.check_in<t_object>(key);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_CheckIn_Single)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    factory.check_in<t_object>(key);
}

BENCHMARK_F(GenericAbstractFactory, Map_CheckIn_Multiple)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    factory.check_in<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_CheckIn_Multiple)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    factory.check_in<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, Map_CheckOut_Single)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key);
      // state.ResumeTiming();
      factory.check_out<t_object>(key);
    }
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_CheckOut_Single)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key);
      // state.ResumeTiming();
      factory.check_out<t_object>(key);
    }
}

BENCHMARK_F(GenericAbstractFactory, Map_CheckOut_Multiple)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    factory.check_out<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_CheckOut_Multiple)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    factory.check_out<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, Map_Create_Single)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    factory.create<t_object>(key);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_Create_Single)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    factory.create<t_object>(key);
}

BENCHMARK_F(GenericAbstractFactory, Map_Create_Multiple)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    factory.create<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_Create_Multiple)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    factory.create<t_object>(key++);
}

BENCHMARK_F(GenericAbstractFactory, Map_Erase_Single)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key);
      // state.ResumeTiming();
      factory.erase(key);
    }
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_Erase_Single)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key);
      // state.ResumeTiming();
      factory.erase<t_object>(key);
    }
}

BENCHMARK_F(GenericAbstractFactory, Map_Erase_Multiple)
(benchmark::State& state)
{
  t_abstract<std::map> factory;

  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    factory.erase(key++);
}

BENCHMARK_F(GenericAbstractFactory, MultiMap_Erase_Multiple)
(benchmark::State& state)
{
  t_abstract<std::multimap> factory;

  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_object>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    factory.erase<t_object>(key++);
}

class AnyAbstractFactory : public benchmark::Fixture
{
 protected:
  template <typename t_key, typename t_tag>
  class AbstractFactory
      : virtual public ::sekreta::internal::api::
            AbstractFactory<t_key, t_tag, std::any, std::multimap>
  {
  };

  template <typename t_key, typename t_tag>
  using Factory = AbstractFactory<t_key, t_tag>;

  using t_key = uint32_t;
  using t_tag = std::string_view;

  Factory<t_key, t_tag> factory;
  t_key key{};

 protected:
  struct One
  {
    static constexpr t_tag type{"one"};
  };

  struct Two
  {
    static constexpr t_tag type{"two"};
  };

  using t_one = One;
  using t_two = Two;
};

BENCHMARK_F(AnyAbstractFactory, CheckIn_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.check_in<t_one>(key);
}

BENCHMARK_F(AnyAbstractFactory, CheckIn_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      factory.check_in<t_one>(key);
      factory.check_in<t_two>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, CheckIn_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      factory.check_in<t_one>(key++);
      factory.check_in<t_two>(key++);
    }
}

BENCHMARK_F(AnyAbstractFactory, CheckOut_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      // state.ResumeTiming();
      factory.check_out<t_one>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, CheckOut_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      factory.check_in<t_two>(key);
      // state.ResumeTiming();
      factory.check_out<t_one>(key);
      factory.check_out<t_two>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, CheckOut_Multiple_MultipleKey)
(benchmark::State& state)
{
  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key++);
      factory.check_in<t_two>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    {
      factory.check_out<t_one>(key++);
      factory.check_out<t_two>(key++);
    }
}

BENCHMARK_F(AnyAbstractFactory, Create_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.create<t_one>(key);
}

BENCHMARK_F(AnyAbstractFactory, Create_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.create<t_one>(key++);
}

BENCHMARK_F(AnyAbstractFactory, Create_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      factory.create<t_one>(key++);
      factory.create<t_two>(key++);
    }
}

BENCHMARK_F(AnyAbstractFactory, Borrow_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      // state.ResumeTiming();
      factory.borrow<t_one>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, Borrow_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      factory.check_in<t_two>(key);
      // state.ResumeTiming();
      factory.borrow<t_one>(key);
      factory.borrow<t_two>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, Borrow_Multiple_MultipleKey)
(benchmark::State& state)
{
  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key++);
      factory.check_in<t_two>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    {
      factory.borrow<t_one>(key++);
      factory.borrow<t_two>(key++);
    }
}

BENCHMARK_F(AnyAbstractFactory, Find)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      // state.ResumeTiming();
      factory.find<t_one>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, View)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      // state.ResumeTiming();
      factory.view();
    }
}

BENCHMARK_F(AnyAbstractFactory, Erase_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      // state.ResumeTiming();
      factory.erase<t_one>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, Erase_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key);
      factory.check_in<t_two>(key);
      // state.ResumeTiming();
      factory.erase<t_one>(key);
      factory.erase<t_two>(key);
    }
}

BENCHMARK_F(AnyAbstractFactory, Erase_Multiple_MultipleKey)
(benchmark::State& state)
{
  key = 0;
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(key++);
      factory.check_in<t_two>(key++);
      // state.ResumeTiming();
    }

  key = 0;
  for (auto _ : state)
    {
      factory.erase<t_one>(key++);
      factory.erase<t_two>(key++);
    }
}

// TODO(anonimal): more benchmarks
