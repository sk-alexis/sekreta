//! \file
//!
//! \brief SWIG interface file to Sekreta's generic components
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details

%module pysek_generic
%{ #include "generic.hh" %}

%include "generic.hh"

//! \todo assignment operator overloading (see build warnings)
