// Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "system.hh"

#include "gtest/gtest.h"

namespace kovri = sekreta::internal::api::kovri;

struct ImplWrapper_KovriFixture : ::testing::Test
{
 protected:
  ImplWrapper_KovriFixture()
      : library(std::make_shared<kovri::impl_wrapper::Library>()),
        path_data(std::make_shared<sekreta::type::kovri::PathData>())
  {
  }

  std::shared_ptr<kovri::impl_wrapper::Library> library;
  std::shared_ptr<sekreta::type::kovri::PathData> path_data;
};

TEST_F(ImplWrapper_KovriFixture, LibraryWrappers)
{
  ASSERT_NO_THROW(kovri::impl_wrapper::Library::Status status(library));
  auto status = std::make_shared<kovri::impl_wrapper::Library::Status>(library);
  ASSERT_NO_THROW(status->lib());
  ASSERT_NE(nullptr, status->lib());

  ASSERT_NO_THROW(kovri::impl_wrapper::Library::Path path(library, path_data));
  auto path =
      std::make_shared<kovri::impl_wrapper::Library::Path>(library, path_data);
  ASSERT_NO_THROW(path->lib());
  ASSERT_NO_THROW(path->path_data());
  ASSERT_NO_THROW(path->impl_data());
  ASSERT_NE(nullptr, path->lib());
  ASSERT_NE(nullptr, path->path_data());
  ASSERT_NE(nullptr, path->impl_data());

  ASSERT_NO_THROW(kovri::impl_wrapper::Library::Path::Status path_status(path));
  auto path_status =
      std::make_shared<kovri::impl_wrapper::Library::Path::Status>(path);
  ASSERT_NO_THROW(path_status->path());
  ASSERT_NE(nullptr, path_status->path());
}

struct Impl_KovriFixture : ::testing::Test
{
 protected:
  class Library final : public kovri::Library
  {
   public:
    Library(const std::shared_ptr<kovri::impl_wrapper::Library>& impl)
        : kovri::Library(impl)
    {
    }

    class Status final : public kovri::Library::Status
    {
     public:
      Status(const std::shared_ptr<kovri::impl_wrapper::Library::Status>& impl)
          : kovri::Library::Status(impl)
      {
      }
    };

    class Path final : public kovri::Library::Path
    {
     public:
      Path(const std::shared_ptr<kovri::impl_wrapper::Library::Path>& impl)
          : kovri::Library::Path(impl)
      {
      }

      class Status final : public kovri::Library::Path::Status
      {
       public:
        Status(
            const std::shared_ptr<kovri::impl_wrapper::Library::Path::Status>&
                impl)
            : kovri::Library::Path::Status(impl)
        {
        }
      };
    };
  };
};

TEST_F(Impl_KovriFixture, Library)
{
  using t_impl = kovri::impl_wrapper::Library;

  auto library_wrapper = std::make_shared<t_impl>();
  ASSERT_NO_THROW(Library library(library_wrapper));

  // Library
  auto library = std::make_shared<Library>(library_wrapper);

  ASSERT_EQ(library->type, sekreta::type::kAPI::Library);
  ASSERT_EQ(library->system, sekreta::type::kSystem::Kovri);
  ASSERT_TRUE(library->library);

  ASSERT_TRUE((library->configure_impl<bool, bool>(true)));
  ASSERT_NO_THROW(library->start_impl<void>());
  ASSERT_NO_THROW(library->restart_impl<void>());
  ASSERT_NO_THROW(library->status_impl<std::shared_ptr<Library::Status>>());
  ASSERT_NO_THROW(library->stop_impl<void>());
  ASSERT_NO_THROW((library->path_impl<
                   std::shared_ptr<Library::Path>,
                   std::shared_ptr<sekreta::type::kovri::PathData>>(
      std::make_shared<sekreta::type::kovri::PathData>())));

  // Status
  auto status_wrapper = std::make_shared<t_impl::Status>(library_wrapper);
  ASSERT_NO_THROW(Library::Status status(status_wrapper));

  auto status = std::make_shared<Library::Status>(status_wrapper);
  ASSERT_EQ(status->type, sekreta::type::kAPI::Status);
  ASSERT_TRUE(status->status);

  using t_nop = bool;
  ASSERT_NO_THROW(status->is_running_impl<t_nop>());
  ASSERT_NO_THROW(status->version_impl<t_nop>());
  ASSERT_NO_THROW(status->data_dir_impl<t_nop>());
  ASSERT_NO_THROW(status->state_impl<t_nop>());
  ASSERT_NO_THROW(status->uptime_impl<t_nop>());
  ASSERT_NO_THROW(status->path_creation_rate_impl<t_nop>());
  ASSERT_NO_THROW(status->path_count_impl<t_nop>());
  ASSERT_NO_THROW(status->active_peer_count_impl<t_nop>());
  ASSERT_NO_THROW(status->known_peer_count_impl<t_nop>());
  ASSERT_NO_THROW(status->inbound_bandwidth_impl<t_nop>());
  ASSERT_NO_THROW(status->outbound_bandwidth_impl<t_nop>());
  ASSERT_NO_THROW(status->floodfill_count_impl<t_nop>());
  ASSERT_NO_THROW(status->leaseset_count_impl<t_nop>());

  // Path
  auto path_wrapper = std::make_shared<t_impl::Path>(
      library_wrapper, std::make_shared<sekreta::type::kovri::PathData>());
  ASSERT_NO_THROW(Library::Path path(path_wrapper));

  auto path = std::make_shared<Library::Path>(path_wrapper);
  ASSERT_EQ(path->type, sekreta::type::kAPI::Path);
  ASSERT_EQ(path->system, sekreta::type::kSystem::Kovri);
  ASSERT_TRUE(path->path);

  ASSERT_NO_THROW(path->configure_impl<void>());
  ASSERT_NO_THROW(path->start_impl<void>());
  ASSERT_NO_THROW(path->restart_impl<void>());
  ASSERT_NO_THROW(path->stop_impl<void>());
  ASSERT_NO_THROW(
      (path->status_impl<std::shared_ptr<Library::Path::Status>>()));

  // Path status
  auto path_status_wrapper =
      std::make_shared<t_impl::Path::Status>(path_wrapper);
  ASSERT_NO_THROW(Library::Path::Status path_status(path_status_wrapper));

  auto path_status =
      std::make_shared<Library::Path::Status>(path_status_wrapper);
  ASSERT_EQ(path_status->type, sekreta::type::kAPI::Status);
  ASSERT_TRUE(path_status->status);

  using t_nop = bool;
  ASSERT_NO_THROW(path_status->is_running_impl<t_nop>());
  ASSERT_NO_THROW(path_status->uptime_impl<t_nop>());
  ASSERT_NO_THROW(path_status->state_impl<t_nop>());
}

// TODO(anonimal): finish WIP branch
