# Copyright (c) 2020, authors of Sekreta. See AUTHORS.md
#       or use the `git log` command on this file.
#
#    Licensed under the Sekreta License, Version 1.0,
#  which inherits from the Apache License, Version 2.0.
#   Combined, they are described as a single "License".
#
#        See the LICENSE.md file for details.

cmake_minimum_required(VERSION 3.10.2...3.16.4)

project(swig)

include(ExternalProject)

ExternalProject_Add(
  swig
  GIT_REPOSITORY    https://github.com/swig/swig
  GIT_TAG           rel-4.0.1
  GIT_SHALLOW       ON
  GIT_PROGRESS      ON
  SOURCE_DIR        "${SWIG_SOURCE_DIR}/src"
  BINARY_DIR        "${SWIG_SOURCE_DIR}/build"
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR> ./autogen.sh
  COMMAND           ${CMAKE_COMMAND} -E chdir <SOURCE_DIR> ./configure --prefix=<BINARY_DIR>
  BUILD_COMMAND     $(MAKE) -C <SOURCE_DIR> -j $(nproc)
  INSTALL_COMMAND   $(MAKE) -C <SOURCE_DIR> install)
