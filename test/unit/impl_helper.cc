// Copyright (c) 2018-2019, authors of Sekreta. See AUTHORS.md
//       or use the `git log` command on this file.
//
//    Licensed under the Sekreta License, Version 1.0,
//  which inherits from the Apache License, Version 2.0.
//   Combined, they are described as a single "License".
//
//         See the LICENSE.md file for details.

#include "impl_helper.hh"

#include "gtest/gtest.h"

namespace impl = sekreta::api::impl_helper;
namespace type = sekreta::api::impl_helper::type;

struct ImplHelper_ArgsFixture : ::testing::Test
{
 protected:
  using t_value = std::string_view;
  using t_daemon = impl::DaemonArgs<t_value>;
  using t_wallet = impl::WalletArgs<t_value>;

  impl::Args<t_value> args;

  template <typename t_location>
  auto parse_args(const std::vector<t_value>& value)
  {
    return impl::parse_args<t_location, t_value>(value);
  };
};

TEST_F(ImplHelper_ArgsFixture, GetKey)
{
  ASSERT_TRUE(args.get_key<type::kLocation>("client"));
  ASSERT_TRUE(args.get_key<type::kLocation>("wallet"));
  ASSERT_TRUE(args.get_key<type::kLocation>("daemon"));
  ASSERT_FALSE(args.get_key<type::kLocation>("N/A"));

  ASSERT_TRUE(args.get_key<type::kCommand>("start"));
  ASSERT_TRUE(args.get_key<type::kCommand>("stop"));
  ASSERT_TRUE(args.get_key<type::kCommand>("restart"));
  ASSERT_TRUE(args.get_key<type::kCommand>("status"));
  ASSERT_FALSE(args.get_key<type::kCommand>("N/A"));

  ASSERT_TRUE(args.get_key<type::kSystem>("kovri"));
  ASSERT_TRUE(args.get_key<type::kSystem>("ire"));
  ASSERT_TRUE(args.get_key<type::kSystem>("i2p"));
  ASSERT_TRUE(args.get_key<type::kSystem>("tor"));
  ASSERT_TRUE(args.get_key<type::kSystem>("loki"));
  ASSERT_FALSE(args.get_key<type::kSystem>("N/A"));

  // static
  ASSERT_FALSE(impl::Args<t_value>::get_key<type::kLocation>("N/A"));
  ASSERT_FALSE(impl::Args<t_value>::get_key<type::kCommand>("N/A"));
  ASSERT_FALSE(impl::Args<t_value>::get_key<type::kSystem>("N/A"));
}

TEST_F(ImplHelper_ArgsFixture, GetValue)
{
  ASSERT_EQ("client", args.get_value<type::kLocation>(type::kLocation::Client));
  ASSERT_EQ("daemon", args.get_value<type::kLocation>(type::kLocation::Daemon));
  ASSERT_EQ("wallet", args.get_value<type::kLocation>(type::kLocation::Wallet));

  ASSERT_EQ("start", args.get_value<type::kCommand>(type::kCommand::Start));
  ASSERT_EQ("stop", args.get_value<type::kCommand>(type::kCommand::Stop));
  ASSERT_EQ("restart", args.get_value<type::kCommand>(type::kCommand::Restart));
  ASSERT_EQ("status", args.get_value<type::kCommand>(type::kCommand::Status));

  ASSERT_EQ("kovri", args.get_value<type::kSystem>(type::kSystem::Kovri));
  ASSERT_EQ("ire", args.get_value<type::kSystem>(type::kSystem::Ire));
  ASSERT_EQ("i2p", args.get_value<type::kSystem>(type::kSystem::I2P));
  ASSERT_EQ("tor", args.get_value<type::kSystem>(type::kSystem::Tor));
  ASSERT_EQ("loki", args.get_value<type::kSystem>(type::kSystem::Loki));

  ASSERT_EQ(
      "Unsupported argument",
      args.get_value<type::kError>(type::kError::Unsupported));
  ASSERT_EQ(
      "Invalid count of arguments",
      args.get_value<type::kError>(type::kError::Count));
  ASSERT_EQ(
      "Invalid location", args.get_value<type::kError>(type::kError::Location));
  ASSERT_EQ(
      "Invalid command", args.get_value<type::kError>(type::kError::Command));
  ASSERT_EQ(
      "Invalid system or system not supported",
      args.get_value<type::kError>(type::kError::System));
  ASSERT_EQ(
      "Invalid address", args.get_value<type::kError>(type::kError::Address));
  ASSERT_EQ(
      "Invalid network or network not supported",
      args.get_value<type::kError>(type::kError::Network));

  // static
  ASSERT_EQ(
      "wallet",
      impl::Args<t_value>::get_value<type::kLocation>(type::kLocation::Wallet));
  ASSERT_EQ(
      "start",
      impl::Args<t_value>::get_value<type::kCommand>(type::kCommand::Start));
  ASSERT_EQ(
      "kovri",
      impl::Args<t_value>::get_value<type::kSystem>(type::kSystem::Kovri));
}

TEST_F(ImplHelper_ArgsFixture, BadArgs)
{
  // Invalid
  auto daemon_args = parse_args<t_daemon>({"N/A"});
  ASSERT_FALSE(daemon_args.first);
  ASSERT_TRUE(daemon_args.second);

  auto wallet_args = parse_args<t_wallet>({"N/A"});
  ASSERT_FALSE(wallet_args.first);
  ASSERT_TRUE(wallet_args.second);

  // Too short
  daemon_args = parse_args<t_daemon>({"start"});
  ASSERT_FALSE(daemon_args.first);
  ASSERT_TRUE(daemon_args.second);

  wallet_args = parse_args<t_wallet>({"daemon", "start"});
  ASSERT_FALSE(wallet_args.first);
  ASSERT_TRUE(wallet_args.second);

  // Invalid location
  wallet_args = parse_args<t_wallet>({"unknown", "restart", "kovri"});
  ASSERT_FALSE(wallet_args.first);
  ASSERT_TRUE(wallet_args.second);

  // Invalid command
  daemon_args = parse_args<t_daemon>({"initialize"});
  ASSERT_FALSE(daemon_args.first);
  ASSERT_TRUE(daemon_args.second);

  wallet_args = parse_args<t_wallet>({"client", "initialize"});
  ASSERT_FALSE(wallet_args.first);
  ASSERT_TRUE(wallet_args.second);

  // Invalid system
  daemon_args = parse_args<t_daemon>({"start", "unknown"});
  ASSERT_FALSE(daemon_args.first);
  ASSERT_TRUE(daemon_args.second);

  wallet_args = parse_args<t_wallet>({"client", "start", "unknown"});
  ASSERT_FALSE(wallet_args.first);
  ASSERT_TRUE(wallet_args.second);
}

TEST_F(ImplHelper_ArgsFixture, GoodArgs)
{
  auto daemon_args = impl::parse_args<t_daemon>(
      {"start", "kovri", "--port=12345", "--enable-upnp"});

  ASSERT_TRUE(daemon_args.first);
  ASSERT_FALSE(daemon_args.second);

  EXPECT_EQ(daemon_args.first->command, "start");
  EXPECT_EQ(daemon_args.first->system, "kovri");
  EXPECT_EQ(daemon_args.first->system_args.at(0), "--port=12345");
  EXPECT_EQ(daemon_args.first->system_args.at(1), "--enable-upnp");

  auto wallet_args = parse_args<t_wallet>(
      {"wallet", "start", "kovri", "--port=12345", "--enable-upnp"});

  ASSERT_TRUE(wallet_args.first);
  ASSERT_FALSE(wallet_args.second);

  EXPECT_EQ(wallet_args.first->location, "wallet");
  EXPECT_EQ(wallet_args.first->command, "start");
  EXPECT_EQ(wallet_args.first->system, "kovri");
  EXPECT_EQ(wallet_args.first->system_args.at(0), "--port=12345");
  EXPECT_EQ(wallet_args.first->system_args.at(1), "--enable-upnp");
}
