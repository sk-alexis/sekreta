//! \file sekretari/ssd.hh
//!
//! \brief Implements the Sekretari SSD component
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \todo Merge WIP branch when in working condition

#ifndef SRC_SEKRETARI_SSD_HH_
#define SRC_SEKRETARI_SSD_HH_

#endif  // SRC_SEKRETARI_SSD_HH_
