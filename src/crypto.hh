//! \file crypto.hh
//!
//! \brief Cryptographical support for Sekreta(ri)
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright Sekreta License, Version 1.0. See the LICENSE.md file for details
//!
//! \details Interfaces, abstractions, and implementations for all
//!   cryptographical material. All "generic" abstractions that are
//!   crypto-related should be stored here.
//!
//! \warning Underlying crypto implementations must never be called directly
//!   outside of this file. Sekreta(ri) must only use these interfaces.
//!
//! \todo Merge WIP branch when in working condition.

#ifndef SRC_CRYPTO_HH_
#define SRC_CRYPTO_HH_

#endif  // SRC_CRYPTO_HH_
